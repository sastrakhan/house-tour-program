﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House
{
    class Outside : Location
    {
        private bool hot;

        public Outside (bool Hotness, string Name)
            : base(Name)
        {
            this.hot = Hotness;
        }

        public override string Description
        { 
            get
            {
                string newDescription = base.Description;
                if (hot)
                {
                    newDescription += " It's very hot here.";
                    return newDescription;
                }
                else
                {
                    return newDescription;
                }
            }
        }
    }
}
