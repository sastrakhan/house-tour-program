﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House
{
    class OutsideWithDoor : Outside, IHasExteriorDoor
    {
        public Location DoorLocation { get; set; }
        public string DoorDescription { get; set; }

        public OutsideWithDoor(string roomTitle, string doorDescription, bool temp) : base(temp, roomTitle)
        {
            this.DoorDescription = doorDescription;
        }

        public override string Description
        {
            get
            {
                return base.Description + "The door opens to the " + DoorLocation.Name ;
            }
        }
    }
}
