﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House
{
    class RoomWithDoor : Room, IHasExteriorDoor
    {
        //Why do we really need DoorLocation if we have it in the base class?  Cause you're inheriting from IHasExteriorDoory
        public Location DoorLocation { get; set; }
        public string DoorDescription { get; set; }

        public RoomWithDoor(string roomTitle, string decoration, string doorDescription) : base(decoration, roomTitle)
        {
            this.DoorDescription = doorDescription;
        }

        public override string Description
        {
            get
            {
                return base.Description + "The door opens to the " + DoorLocation.Name;
            }
        }
    }
}
