﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House
{
    public partial class Form1 : Form
    {
        Location currentLocation;
        public Form1()
        {
            InitializeComponent();
            CreateObjects();
            MoveToANewLocation(currentLocation);
        }

        private void CreateObjects()
        {
            RoomWithDoor livingRoom = new RoomWithDoor("Living Room", "an antique carpet", "an oak door with a brass knob");
            Room diningRoom = new Room("crystal chandelier", "Dining Room");
            RoomWithDoor kitchen = new RoomWithDoor("Kitchen", "stainless steel sh*t", "screen door backyard");
            OutsideWithDoor frontYard = new OutsideWithDoor("Front Yard", "an oak door to Lroom", true);
            OutsideWithDoor backYard = new OutsideWithDoor("Back Yard", "screen door to garden", false);
            Outside garden = new Outside(true, "Garden");
            Outside allRooms = new Outside(false, "All Rooms");

            livingRoom.Exits = new Location[] { frontYard, diningRoom };
            diningRoom.Exits = new Location[] { livingRoom, kitchen };
            kitchen.Exits = new Location[] { diningRoom, backYard };
            frontYard.Exits = new Location[] { livingRoom, garden };
            backYard.Exits = new Location[] { kitchen, garden };
            garden.Exits = new Location[] { frontYard, backYard };
            //You're supposed to enter the house from the living room but I added the allRooms object so you start the tour from wherever you want
            allRooms.Exits = new Location[] { frontYard, livingRoom, diningRoom, kitchen, frontYard, backYard, garden };

            livingRoom.DoorLocation = frontYard;
            kitchen.DoorLocation = backYard;
            frontYard.DoorLocation = livingRoom;
            backYard.DoorLocation = kitchen;

            currentLocation = allRooms;
            description.Text = currentLocation.Description;
        }

        private void goHere_Click(object sender, EventArgs e)
        {
            MoveToANewLocation(currentLocation.Exits[comboBox1.SelectedIndex]);
            
        }

        //If you set MoveToANewLocation to public it gives the error "Error	1	Inconsistent accessibility: parameter type 'House.Location' is less accessible than method 'House.Form1.MoveToANewLocation(House.Location)'	C:\Users\big-foot\Documents\Visual Studio 2013\Projects\House\House\Form1.cs	58	21	House
        //why? 
        private void MoveToANewLocation(Location location)
        {
            currentLocation = location;
            comboBox1.Items.Clear();
            for (int i = 0; i < currentLocation.Exits.Length; i++)
            {
                comboBox1.Items.Add(currentLocation.Exits[i].Name);
                comboBox1.SelectedIndex = 0;
            }

            /*I added an override to the Description method in the RoomWithDoor and OutsideWithDoor classes so they tell you where the door leads to.  
            Why are they able to be called below if currentLocation is only a Location object, which doesn't have access to it's child classes' methods right?
            I thought I would have to add some conditionals like 
            if (currentLocation is RoomWithDoor){
                var hasDoor = currentLocation as RoomWithDoor; 
                description.Text = hasDoor.Description;
            }*/
            description.Text = currentLocation.Description; 

            if (currentLocation is IHasExteriorDoor)
            {
                goThroughDoor.Visible = true;
            }
            else
            {
                goThroughDoor.Visible = false;
            }
           
        }

        private void goThroughDoor_Click(object sender, EventArgs e)
        {
                IHasExteriorDoor tempHasDoor = currentLocation as IHasExteriorDoor;
                MoveToANewLocation(tempHasDoor.DoorLocation);
        }
    }
}
