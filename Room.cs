﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House
{
    class Room : Location
    {
        private string decoration;
        public string Decoration { get { return decoration; } set{decoration = value;}}

        public Room(string decoration, string roomName) : base(roomName)
        {
            this.Decoration = decoration;
        }

        public override string Description
        {
            get
            {
                String description = base.Description;
                description += "  You also see " + Decoration;
                return description;
            }
        }
    }
}
